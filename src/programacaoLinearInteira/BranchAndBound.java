/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package programacaoLinearInteira;

import programacaoLinear.SimplexDuasFases;
import programacaoLinear.Solucao;

/**
 *
 * @author Matheus
 */
public class BranchAndBound {

    private String tipo;
    private double[] funcao;
    private double[][] restricoes;
    private double[] resultadosRetricoes;
    private String[] condicoes;

    /**
     * Construtor da classe Branch And Bound.
     *
     * @param tipo
     * @param funcao
     * @param restricoes
     * @param resultadoRetricoes
     * @param condicoes
     */
    public BranchAndBound(String tipo, double[] funcao, double[][] restricoes, double[] resultadoRetricoes, String[] condicoes) {
        this.tipo = tipo;
        this.funcao = funcao;
        this.restricoes = restricoes;
        this.resultadosRetricoes = resultadoRetricoes;
        this.condicoes = condicoes;
    }

    /**
     * Metodo que calcula a solucao peelo metodo Branch And Bound.
     *
     * @return
     */
    public Solucao calcular() {

        // Calculando resultado pelo simplex duas fases.
        SimplexDuasFases simplexDuasFases = new SimplexDuasFases(tipo, funcao, restricoes, resultadosRetricoes, condicoes);
        Solucao solucao = simplexDuasFases.calcular();
        if (solucao == null) {
            return null;
        }
        solucao.imprimir();
        // Contador de variaveis inteiras da solucao.
        int contVarSolucaoInteira = 0;
        // Percorrendo vetor de variaveis da solucao.
        for (int i = 0; i < solucao.getVariaveis().length; i++) {
            double valor = 0;
            int valorInteiro = 0;
            double resto = 0;

            // Verificando se os resultados sao inteiros.
            valor = solucao.getVariaveis()[i];
            valorInteiro = (int) (solucao.getVariaveis()[i]);
            resto = valor - valorInteiro;
            if (resto == 0) {
                // O valor e inteiro.
                contVarSolucaoInteira++;
            } else {
                // O valor nao e inteiro.
                double valorEsquerda = valorInteiro;
                double valorDireita = valorInteiro + 1;
                System.out.println("Nó esquerda: " + valorEsquerda);
                System.out.println("Nó Direita: " + valorDireita);

                // Partindo para o no da esquerda:
                Solucao noEsquerda = noEsquerda(i, valorEsquerda);
                Solucao noDireita = noDireita(i, valorDireita);
                if ((noEsquerda == null) && (noDireita == null)) {

                    return null;
                } else if (noEsquerda == null) {
                    return noDireita;
                } else if (noDireita == null) {
                    return noEsquerda;
                } else {
                    if (tipo.equals("min")) {
                        if (noEsquerda.getResultado() < noDireita.getResultado()) {
                            return noEsquerda;
                        } else {
                            return noDireita;
                        }
                    } else {
                        if (noEsquerda.getResultado() > noDireita.getResultado()) {
                            return noEsquerda;
                        } else {
                            return noDireita;
                        }
                    }
                }
            }
        }
        return solucao;
    }

    /**
     * Metodo que implementa a solucao da esquerda.
     *
     * @return
     */
    private Solucao noEsquerda(int variavel, double valor) {
        // Criando nova Restricao.
        double[][] novoRestricoes = new double[restricoes.length + 1][restricoes[0].length];
        for (int i = 0; i < novoRestricoes.length; i++) {
            for (int j = 0; j < novoRestricoes[0].length; j++) {
                if (i == novoRestricoes.length - 1) {
                    if (j == variavel) {
                        novoRestricoes[i][j] = 1;
                    } else {
                        novoRestricoes[i][j] = 0;
                    }
                } else {
                    novoRestricoes[i][j] = restricoes[i][j];
                }
            }
        }
        // Criando novo resultado da restricao.
        double[] novoResultadosRestricoes = new double[resultadosRetricoes.length + 1];
        for (int i = 0; i < resultadosRetricoes.length; i++) {
            novoResultadosRestricoes[i] = resultadosRetricoes[i];
        }
        novoResultadosRestricoes[novoResultadosRestricoes.length - 1] = valor;
        // Criando novo condicoes.
        String[] novoCondicoes = new String[condicoes.length + 1];
        for (int i = 0; i < condicoes.length; i++) {
            novoCondicoes[i] = condicoes[i];
        }
        novoCondicoes[novoCondicoes.length - 1] = "<=";
        // Calculando simplex duas fases.
        BranchAndBound bAB = new BranchAndBound(tipo, funcao, novoRestricoes, novoResultadosRestricoes, novoCondicoes);
        Solucao solucao = bAB.calcular();

        return solucao;
    }

    /**
     * Metodo que implementa a solucao da direita.
     *
     * @return
     */
    private Solucao noDireita(int variavel, double valor) {
        // Criando nova Restricao.
        double[][] novoRestricoes = new double[restricoes.length + 1][restricoes[0].length];
        for (int i = 0; i < novoRestricoes.length; i++) {
            for (int j = 0; j < novoRestricoes[0].length; j++) {
                if (i == novoRestricoes.length - 1) {
                    if (j == variavel) {
                        novoRestricoes[i][j] = 1;
                    } else {
                        novoRestricoes[i][j] = 0;
                    }
                } else {
                    novoRestricoes[i][j] = restricoes[i][j];
                }
            }
        }
        // Criando novo resultado da restricao.
        double[] novoResultadosRestricoes = new double[resultadosRetricoes.length + 1];
        for (int i = 0; i < resultadosRetricoes.length; i++) {
            novoResultadosRestricoes[i] = resultadosRetricoes[i];
        }
        novoResultadosRestricoes[novoResultadosRestricoes.length - 1] = valor;
        // Criando novo condicoes.
        String[] novoCondicoes = new String[condicoes.length + 1];
        for (int i = 0; i < condicoes.length; i++) {
            novoCondicoes[i] = condicoes[i];
        }
        novoCondicoes[novoCondicoes.length - 1] = ">=";
        // Calculando simplex duas fases.
        BranchAndBound bAB = new BranchAndBound(tipo, funcao, novoRestricoes, novoResultadosRestricoes, novoCondicoes);
        Solucao solucao = bAB.calcular();

        return solucao;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package principal;

import programacaoLinear.Simplex;
import programacaoLinear.SimplexDuasFases;
import programacaoLinear.Solucao;
import programacaoLinearInteira.BranchAndBound;

/**
 *
 * @author Matheus
 */
public class Principal {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        /**
         * Simplex: Maximizar.
         */
//        double[][] maximizar = {
//            {1, 0, 1, 0, 0, 4},
//            {0, 2, 0, 1, 0, 12},
//            {3, 7, 0, 0, 1, 18},
//            {-3, -5, 0, 0, 0, 0}
//        };
//        Simplex simplexMax = new Simplex(3, 2, "max", maximizar);
//        Solucao s = simplexMax.calcular();
//        s.imprimir();
        /**
         * Simplex: Minimizar.
         */
//        double[][] minimizar = {
//            {2, 1, 1, 0, 10},
//            {1, -1, 0, 1, 8},
//            {-4, 2, 0, 0, 0}
//        };
//        Simplex simplexMin = new Simplex(2, 2, "min", minimizar);
//        Solucao s = simplexMin.calcular();
//        s.imprimir();
        /**
         * Simplex Duas Fases: Problema 5.
         */
//        double[] funcao = {-2, -2, -4};
//        double[][] restricoes = {{2, 1, 1}, {3, 4, 2}};
//        double[] resultadoRestricoes = {2, 8};
//        String[] condicoes = {"<=", ">="};
//
//        SimplexDuasFases duasFases = new SimplexDuasFases("max", funcao, restricoes, resultadoRestricoes, condicoes);
//        Solucao solucao = duasFases.calcular();
//        solucao.imprimir();
        /**
         * Simplex Duas Fases: Problema 6.
         */
//        double[] funcao = {-3, -2, -3};
//        double[][] restricoes = {{2, 1, 1}, {1, 3, 1}, {3, 4, 2}};
//        double[] resultadoRestricoes = {2, 6, 8};
//        String[] condicoes = {"=", "=", "="};
//
//        SimplexDuasFases duasFases = new SimplexDuasFases("max", funcao, restricoes, resultadoRestricoes, condicoes);
//        Solucao solucao = duasFases.calcular();
//        solucao.imprimir();
        /**
         * Simplex Duas Fases: Problema 7. //
         */
//        double[] funcao = {-3, -2, -3};
//        double[][] restricoes = {{2, 1, 1}, {3, 4, 2}};
//        double[] resultadoRestricoes = {2, 8};
//        String[] condicoes = {"<=", ">="};
//
//        SimplexDuasFases duasFases = new SimplexDuasFases("max", funcao, restricoes, resultadoRestricoes, condicoes);
//        Solucao solucao = duasFases.calcular();
//        solucao.imprimir();
        /**
         * Branch and Bound: Problema Facil.
         */
//        double[] funcao = {-1, -4};
//        double[][] restricoes = {{-2, 4}, {2, 3}};
//        double[] resultadoRestricoes = {8, 12};
//        String[] condicoes = {"<=", "<="};
//        BranchAndBound bAB = new BranchAndBound("max", funcao, restricoes, resultadoRestricoes, condicoes);
//        Solucao solucao = bAB.calcular();
//        if (solucao != null) {
//            System.out.println("\n+-----------------------SOLUCAO INTEIRA B&B-----------------------+");
//            solucao.imprimir();
//        } else {
//            System.out.println("Solucao inteira nao viável!");
//        }
        /**
         * Branch and Bound: Problema Medio.
         */
//        double[] funcao = {-5, -8};
//        double[][] restricoes = {{1, 1}, {5, 9}};
//        double[] resultadoRestricoes = {6, 45};
//        String[] condicoes = {"<=", "<="};
//        BranchAndBound bAB = new BranchAndBound("max", funcao, restricoes, resultadoRestricoes, condicoes);
//        Solucao solucao = bAB.calcular();
//        if (solucao != null) {
//            System.out.println("\n+-----------------------SOLUCAO INTEIRA B&B-----------------------+\n");
//            solucao.imprimir();
//        } else {
//            System.out.println("Solucao inteira nao viável!");
//        }
        /**
         * Branch and Bound: Problema Dificil.
         */
        double[] funcao = {-6, -8};
        double[][] restricoes = {{6, 7}, {0, 1}};
        double[] resultadoRestricoes = {40, 2};
        String[] condicoes = {">=", ">="};
        BranchAndBound bAB = new BranchAndBound("min", funcao, restricoes, resultadoRestricoes, condicoes);
        Solucao solucao = bAB.calcular();
        if (solucao != null) {
            System.out.println("\n+-----------------------SOLUCAO INTEIRA B&B-----------------------+\n");
            solucao.imprimir();
        } else {
            System.out.println("Solucao inteira nao viável!");
        }

    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package programacaoLinear;

/**
 *
 * @author Matheus
 */
public class Simplex {

    private int qtdRestricoes;
    private int qtdVariaveis;
    private String tipo;
    private int variaveis[];
    private double valorVariaveis[];
    private int linhas;
    private int colunas;
    private double[][] tableau;
    private boolean solucaoIlimitada;

    /**
     * Metodo construtor da classe.
     *
     * @param qtdRestricoes
     * @param qtdVariaveis
     * @param tipo = max ou min
     * @param matriz
     */
    public Simplex(int qtdRestricoes, int qtdVariaveis, String tipo, double[][] matriz) {
        this.solucaoIlimitada = false;
        this.qtdRestricoes = qtdRestricoes;
        this.qtdVariaveis = qtdVariaveis;
        this.tipo = tipo;
        // Inicializando vetor com a informacao das variaveis referentes as linhas das variaveis da solucao otima.
        this.variaveis = new int[qtdRestricoes];
        for (int i = 0; i < variaveis.length; i++) {
            variaveis[i] = qtdVariaveis + i;
        }
        // Inicializando vetor com valor das variaveis finais.
        this.valorVariaveis = new double[qtdVariaveis];
        for (int i = 0; i < valorVariaveis.length; i++) {
            valorVariaveis[i] = 0;
        }
        // Instanciando e copiando tableau.
        this.linhas = matriz.length;
        this.colunas = matriz[0].length;
        this.tableau = new double[linhas][colunas];
        for (int i = 0; i < tableau.length; i++) {
            System.arraycopy(matriz[i], 0, this.tableau[i], 0, matriz[i].length);
        }

        // Inverte z para problemas de minimização.
        if (tipo.equals("min")) {
            for (int i = 0; i < colunas; i++) {
                if (tableau[linhas - 1][i] != 0) {
                    tableau[linhas - 1][i] = tableau[linhas - 1][i] * -1;
                }
            }
        }
    }

    /**
     * Metodo que calcula os valores do tableau pelo metodo simplex.
     *
     * @return
     */
    public Solucao calcular() {

        int iteracao = 0;
        while (true) {

            System.out.println((iteracao + 1) + "º iteração:\n");
            iteracao++;
            imprimirTableau();

            // Passo 1: Checa se a solução e otima. Caso sim, entao finaliza metodo.
            if (checaSolucaoOtima()) {
                // Montando vetor de variaveis.
                for (int i = 0; i < variaveis.length; i++) {
                    if ((variaveis[i] < qtdVariaveis) && (variaveis[i] != -1)) {
                        valorVariaveis[variaveis[i]] = tableau[i][tableau[0].length - 1];
                    }
                }
                // Invertendo funcao objetivo caso a funcao seja de minimizar.
                if (tipo.equals("min")) {
                    for (int i = 0; i < colunas; i++) {
                        tableau[linhas - 1][i] = tableau[linhas - 1][i] * -1;
                    }
                }
                return new Solucao(tableau, valorVariaveis);
            }

            // Passo 2: Encontrar coluna pivo.
            int colunaPivo = Simplex.this.encontrarColunaPivo();

            // Passo 3: Encontrar a linha pivo a partir da coluna pivo.
            int linhaPivo = encontrarLinhaPivo(colunaPivo);
            // Passo 3.1: Verifica se a solucao e ilimitada.
            if (solucaoIlimitada) {
                System.out.println("---Solucao e ilimitada---");
                return null;
            }

            // Passo 4: Cria proximo tableau.
            System.out.println("Linha pivo: " + linhaPivo);
            System.out.println("Coluna pivo:" + colunaPivo);
            System.out.println("Pivo: " + String.format("%.2f", tableau[linhaPivo][colunaPivo]) + "\n");

            criarProximoTableau(linhaPivo, colunaPivo);

        }
    }

    /**
     * Verifica se o tableau contem a solucao otima.
     *
     * @return
     */
    private boolean checaSolucaoOtima() {
        boolean Otima = false;
        int vCount = 0;

        for (int i = 0; i < colunas - 1; i++) {
            double val = tableau[linhas - 1][i];
            if (val >= 0) {
                vCount++;
            }
        }

        if (vCount == colunas - 1) {
            Otima = true;
        }
        return Otima;
    }

    /**
     * Metodo que encontra proxima coluna que entra na base. Encontra o mais
     * negativo.
     *
     * @return
     */
    private int encontrarColunaPivo() {
        double maisNegativo = tableau[linhas - 1][0];
        int posicao = 0;
        for (int i = 0; i < colunas - 1; i++) {
            if (tableau[linhas - 1][i] < maisNegativo) {
                maisNegativo = tableau[linhas - 1][i];
                posicao = i;
            }
        }
        return posicao;
    }

    /**
     * Metodo que encontra a linha pivo.
     *
     * @param column
     * @return
     */
    private int encontrarLinhaPivo(int column) {
        int posicao = -1;
        double minimo = -1;
        double[] entradas = new double[linhas];
        int qtdNegativo = 0;
        for (int i = 0; i < linhas; i++) {
            if (tableau[i][column] > 0) {
                entradas[i] = tableau[i][column];
            } else {
                entradas[i] = tableau[i][column];
                qtdNegativo++;
            }
        }

        // Solucao e limitada caso todos os campos da coluna pivo seja negativo.
        if (qtdNegativo == linhas) {
            solucaoIlimitada = true;
        } else {
            // Encontra o menor valor positivo da razao.
            for (int i = 0; i < qtdRestricoes; i++) {
                if (variaveis[i] >= qtdVariaveis) {
                    double val = entradas[i];
                    if (val > 0) {
                        if (minimo < 0) {
                            minimo = tableau[i][colunas - 1] / val;
                            posicao = i;
                        } else {
                            double teste = tableau[i][colunas - 1] / val;
                            if ((teste >= 0) && (teste < minimo)) {
                                minimo = teste;
                                posicao = i;
                            }
                        }
                    } else {
                        if (tableau[i][colunas - 1] == 0) {
                            minimo = 0;
                            posicao = i;
                        }
                    }
                }

            }
        }
        return posicao;
    }

    /**
     * Metodo que forma um novo tableau.
     *
     * @param linhaPivo
     * @param ColunaPivo
     */
    private void criarProximoTableau(int linhaPivo, int ColunaPivo) {
        double pivo = tableau[linhaPivo][ColunaPivo];
        double[] valoresLinhaPivo = new double[colunas];
        double[] valoresColunaPivo = new double[colunas];
        double[] novaLinha = new double[colunas];

        // Divide todas as entradas na linha de pivo por entrada na coluna pivo.
        // Obtém entrada na linha de pivo.
        System.arraycopy(tableau[linhaPivo], 0, valoresLinhaPivo, 0, colunas);

        // Obter entrada na coluna pivo.
        for (int i = 0; i < linhas; i++) {
            valoresColunaPivo[i] = tableau[i][ColunaPivo];
        }

        // Divide os valores na linha pivo pelo pivo.
        for (int i = 0; i < colunas; i++) {
            novaLinha[i] = valoresLinhaPivo[i] / pivo;
        }

        // Subtrai de cada uma das outras linhas.
        for (int i = 0; i < linhas; i++) {
            if (i != linhaPivo) {
                for (int j = 0; j < colunas; j++) {
                    double c = valoresColunaPivo[i];
                    tableau[i][j] = tableau[i][j] - (c * novaLinha[j]);
                }
            } else {
                variaveis[i] = ColunaPivo;
            }
        }

        // Substitui a linha.
        System.arraycopy(novaLinha, 0, tableau[linhaPivo], 0, novaLinha.length);
    }

    /**
     * Metodo que imprime o tableau na tela.
     */
    public void imprimirTableau() {
        for (int i = 0; i < linhas; i++) {
            for (int j = 0; j < colunas; j++) {
                String value = String.format("%.2f", tableau[i][j]);
                System.out.print(value + "\t");
            }
            System.out.println();
        }
        System.out.println();
    }

    /**
     * Metodo que retorna o tableau apos verificar solucao otima.
     *
     * @return
     */
    public double[][] getTableau() {
        return tableau;
    }

    /**
     * Metodo que retorna as variaveis do tableau.
     *
     * @return
     */
    public int[] getVariaveis() {
        return variaveis;
    }

    /**
     * Metodo que as variaveis no tableau.
     *
     * @param variaveis
     */
    public void setVariaveis(int[] variaveis) {
        this.variaveis = variaveis;
    }

    /**
     * Metodo que retorna o valor das variaveis.
     *
     * @return
     */
    public double[] getValorVariaveis() {
        return valorVariaveis;
    }

    /**
     * Metodo que insere valores das variaveis da solucao otima. Utilizado
     * quando se vai utilizar o simplex duas fase.
     *
     * @param valorVariaveis
     */
    public void setValorVariaveis(double[] valorVariaveis) {
        this.valorVariaveis = valorVariaveis;
    }

}

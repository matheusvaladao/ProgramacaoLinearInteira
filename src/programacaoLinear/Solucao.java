/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package programacaoLinear;

/**
 * Classe que implementa a solucao de um problema linear.
 *
 * @author Matheus
 */
public class Solucao {

    private double[][] tableau;
    private double[] variaveis;
    private double resultado;

    public Solucao(double[][] tableau, double[] variaveis) {
        this.tableau = tableau;
        this.variaveis = variaveis;
        this.resultado = tableau[tableau.length - 1][tableau[0].length - 1];
    }

    public double[][] getTableau() {
        return tableau;
    }

    public void setTableau(double[][] tableau) {
        this.tableau = tableau;
        this.resultado = tableau[tableau.length - 1][tableau[0].length - 1];
    }

    public double[] getVariaveis() {
        return variaveis;
    }

    public void setVariaveis(double[] variaveis) {
        this.variaveis = variaveis;
    }

    public double getResultado() {
        return resultado;
    }

    public void setResultado(double resultado) {
        this.resultado = resultado;
    }

    public void imprimir() {

        System.out.println("\n\n+-----------------------------SOLUÇÃO-----------------------------+\n");
        System.out.println("Tableau:");
        for (int i = 0; i < tableau.length; i++) {
            for (int j = 0; j < tableau[0].length; j++) {
                String value = String.format("%.2f", tableau[i][j]);
                System.out.print(value + "\t");
            }
            System.out.println();
        }

        String variaveisS = "[";
        for (int j = 0; j < variaveis.length; j++) {
            variaveisS = variaveisS + String.format("%.2f", variaveis[j]) + ", ";
        }
        variaveisS = variaveisS.substring(0, variaveisS.length() - 2);
        variaveisS = variaveisS + "]";

        System.out.println("\nVariaveis:");
        System.out.println(variaveisS);

        System.out.println("\nResultado: " + String.format("%.2f", resultado));
        System.out.println("\n\n+-----------------------------------------------------------------+\n");

    }

}

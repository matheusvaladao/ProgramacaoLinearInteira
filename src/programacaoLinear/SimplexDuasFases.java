/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package programacaoLinear;

/**
 *
 * @author Matheus
 */
public class SimplexDuasFases {

    private String tipo;
    private double[] funcao;
    private double[][] restricoes;
    private double[] resultadoRetricoes;
    private String[] condicoes;
    private int qtdVariaveisArtificiais;
    private int[] variaveis;

    /**
     * Metodo construtor da classe.
     *
     * @param tipo
     * @param funcao
     * @param restricoes
     * @param resultadoRetricoes
     * @param condicoes
     */
    public SimplexDuasFases(String tipo, double[] funcao, double[][] restricoes, double[] resultadoRetricoes, String[] condicoes) {
        this.tipo = tipo;
        this.funcao = funcao;
        this.restricoes = restricoes;
        this.resultadoRetricoes = resultadoRetricoes;
        this.condicoes = condicoes;
        this.qtdVariaveisArtificiais = 0;
        this.variaveis = new int[restricoes.length];
    }

    /**
     * Metodo que faz o calculo pelo simplex duas fases.
     *
     * @return
     */
    public Solucao calcular() {

        System.out.println("+-----------------------------PROBLEMA-----------------------------+\n");

        System.out.println("+--------------Primeira Fase--------------+\n");
        // Montando tableau da 1º fase.
        double[][] tableauFase1 = prepararTableauFase1();
        // Realizando Primeira Fase
        Simplex fase1 = new Simplex(restricoes.length, restricoes[0].length, "min", tableauFase1);
        fase1.setVariaveis(variaveis);
        Solucao solucaoFase1 = fase1.calcular();
        if (solucaoFase1 == null) {
            return null;
        }
        // Verificando se a solucao e viavel ou nao caso a soma das variaveis artificais seja diferente de zero.
        if (solucaoFase1.getTableau()[solucaoFase1.getTableau().length - 1][solucaoFase1.getTableau()[0].length - 1] != 0) {
            return null;
        }

        System.out.println("+--------------Segunda Fase--------------+\n");
        // Preparando tableau para fase 2.
        double[][] tableauFase2 = prepararTableauFase2(solucaoFase1.getTableau());
        // Realizando Segunda Fase
        Simplex fase2 = new Simplex(restricoes.length, restricoes[0].length, "max", tableauFase2);
        fase2.setVariaveis(fase1.getVariaveis());
        fase2.setValorVariaveis(fase1.getValorVariaveis());
        Solucao solucaoFase2 = fase2.calcular();
        if (solucaoFase2 == null) {
            return null;
        }
        // Caso a funcao seja de minimizar, entao inverte a linha da variavel z para vizualizacao do resultado.
        if (tipo.equals("min")) {
            for (int i = 0; i < solucaoFase2.getTableau()[0].length; i++) {
                solucaoFase2.getTableau()[solucaoFase2.getTableau().length - 1][i] = solucaoFase2.getTableau()[solucaoFase2.getTableau().length - 1][i] * -1;
            }
            solucaoFase2.setTableau(solucaoFase2.getTableau());
        }

        return solucaoFase2;
    }

    /**
     * Metodo que prepara o tableau para realizar a primeira fase.
     *
     * @return
     */
    private double[][] prepararTableauFase1() {

        // Verificando quantidade de restricoes igual e maior igual para inicializar contador de restricoes igual e de variaveis artificais.
        int qtdCondicaoIgual = 0;
        for (int i = 0; i < condicoes.length; i++) {
            if ((condicoes[i]).equals(">=") || (condicoes[i].equals(">") || (condicoes[i]).equals("="))) {
                qtdVariaveisArtificiais++;
            }
            if (condicoes[i].equals("=")) {
                qtdCondicaoIgual++;
            }
        }

        // Criando tableau da primeira fase.
        double tableauFase1[][] = new double[condicoes.length + 2][restricoes[0].length + condicoes.length + qtdVariaveisArtificiais - qtdCondicaoIgual + 1];

        // Preenchendo tableau com zeros.
        for (int i = 0; i < tableauFase1.length; i++) {
            for (int j = 0; j < tableauFase1[0].length; j++) {
                tableauFase1[i][j] = 0;
            }
        }

        //Inicializando contadores de variaveis de folda e de variaveis artificiais.
        int contFolga = 0;
        int contVarArt = qtdVariaveisArtificiais;
        // Preenchendo tableau da primeira fase.
        for (int i = 0; i < tableauFase1.length - 1; i++) {
            for (int j = 0; j < tableauFase1[0].length; j++) {
                // Se a linha e a da funcao z objetivo.
                if (i == tableauFase1.length - 2) {

                    if (j < funcao.length) {
                        // Inverte valores caso a funcao seja de minimizacao.
                        if (tipo.equals("min")) {
                            tableauFase1[i][j] = funcao[j] * -1;
                        } else {
                            tableauFase1[i][j] = funcao[j];
                        }
                    }

                    // Se a linha nao e da funcao objetivo z.
                } else {
                    // Adiciona restricoes.
                    if (j < restricoes[0].length) {
                        tableauFase1[i][j] = restricoes[i][j];
                    }
                    // Adiciona folga e variaveis artificiais caso tenha e os resultados.
                    if (j >= restricoes[0].length) {
                        // Caso restricao seja de maior igual ou igual(>= || =).
                        if ((condicoes[i]).equals(">=") || (condicoes[i].equals(">") || (condicoes[i]).equals("="))) {
                            // Adiciona variaveis de excesso, variaveis artificiais e resultado das restricoes.
                            if (i < variaveis.length) {
                                variaveis[i] = tableauFase1[0].length - 1 - contVarArt;
                            }
                            tableauFase1[i][j + contFolga] = -1;
                            tableauFase1[i][tableauFase1[0].length - 1 - contVarArt] = 1;
                            j = tableauFase1[0].length - 1;
                            tableauFase1[i][j] = resultadoRetricoes[i];
                            // Soma na ultima linha as linhas das variaveis artificias a funcao objetivo das variaveis artificiais na primeira fase.
                            for (int k = 0; k < tableauFase1[0].length; k++) {
                                if (k != tableauFase1[0].length - 1 - contVarArt) {
                                    tableauFase1[tableauFase1.length - 1][k] = tableauFase1[tableauFase1.length - 1][k] + tableauFase1[i][k];
                                }
                            }
                            contVarArt--;
                            // Caso restricao seja de menor igual (<=).
                        } else {
                            // Adiciona variaveis de sobra e resultado da restricao.
                            if (i < variaveis.length) {
                                variaveis[i] = j + contFolga;
                            }
                            tableauFase1[i][j + contFolga] = 1;
                            j = tableauFase1[0].length - 1;
                            tableauFase1[i][j] = resultadoRetricoes[i];
                        }
                        contFolga++;
                    }
                }

            }
        }
        return tableauFase1;
    }

    /**
     * Metodo que prepara o tableau para realizar a segunda fase.
     *
     * @param tableauFase1
     * @return
     */
    private double[][] prepararTableauFase2(double[][] tableauFase1) {
        // Criando tableau da segunda fase.
        double[][] tableauFase2 = new double[tableauFase1.length - 1][tableauFase1[0].length - qtdVariaveisArtificiais];
        // Preenchendo tabeleau com zeros.
        for (int i = 0; i < tableauFase2.length; i++) {
            for (int j = 0; j < tableauFase2[0].length; j++) {
                tableauFase2[i][j] = 0;
            }
        }

        // Preenchendo com valores da tabela anterior, retirando variaveis artificiais e sua respectiva funcao objetivo da primeira fase.
        for (int i = 0; i < tableauFase2.length; i++) {
            for (int j = 0; j < tableauFase2[0].length; j++) {
                if (j == tableauFase2[0].length - 1) {
                    tableauFase2[i][j] = tableauFase1[i][tableauFase1[0].length - 1];
                } else {
                    tableauFase2[i][j] = tableauFase1[i][j];
                }
            }
        }
        return tableauFase2;
    }
}
